import cookie from 'js-cookie';
import Router from 'next/router';

export const setCookie = (key, value) => {
    if (process.browser) {
        cookie.set(key, value, {
            expires: 1      // day
        });
    }
};

export const removeCookie = key => {
    if (process.browser) {
        cookie.remove(key);
    }
};

// cookie.get('token')
export const getCookie = key => {
    if (process.browser) {
        return cookie.get(key);
    }
};

export const setLocalStorage = (key, value) => {
    if (process.browser) {
        localStorage.setItem(key, JSON.stringify(value));
    }
};

export const removeLocalStorage = key => {
    if (process.browser) {              // vs .server  window
        localStorage.removeItem(key);
    }
};

//                                      cb 
export const authenticate = (response, next) => {
    setCookie('token', response.data.token);
    setLocalStorage('user', response.data.user);
    next();                           // cb 
};

export const isAuth = () => {
    if (process.browser) {
        const cookieChecked = getCookie('token');           // 1st level
        if (cookieChecked) {
            if (localStorage.getItem('user')) {             // 2nd level of security
                return JSON.parse(localStorage.getItem('user'));
            } else {
                return false;
            }
        }
    }
};

export const logout = () => {
    removeCookie('token');
    removeLocalStorage('user');
    Router.push('/login');
};
