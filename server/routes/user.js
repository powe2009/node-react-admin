const express = require('express');
const router = express.Router();

const { getProfile } = require('../controllers/user');
const { requireSignin, authMiddleware, adminMiddleware } = require('../controllers/auth');

router.get('/user', requireSignin, authMiddleware, getProfile);
router.get('/admin', requireSignin, adminMiddleware, getProfile);

module.exports = router;
